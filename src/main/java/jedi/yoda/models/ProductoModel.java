package jedi.yoda.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "producto")
public class ProductoModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long idProducto;

    private String nombreProducto;
    private Integer precioProducto; 
    private Integer cantidadProducto;
    private String descripcionProducto;

    public String getNombreProducto() {
        return nombreProducto;
    }
    public String getDescripcionProducto() {
        return descripcionProducto;
    }
    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }
    public Integer getCantidadProducto() {
        return cantidadProducto;
    }
    public void setCantidadProducto(Integer cantidadProducto) {
        this.cantidadProducto = cantidadProducto;
    }
    public Integer getPrecioProducto() {
        return precioProducto;
    }
    public void setPrecioProducto(Integer precioProducto) {
        this.precioProducto = precioProducto;
    }
    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

}

package jedi.yoda.models;

import javax.persistence.*;

@Entity
@Table(name = "cliente")
public class ClienteModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    
    private String nombre;
    private String apellido;
    private String direccion;
    private Integer cedula;
    private Integer nContacto;
    private Integer codigoVendedor;

    public String getApellido() {
        return apellido;
    }

    public Integer getCodigoVendedor() {
        return codigoVendedor;
    }
    public void setCodigoVendedor(Integer codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }
    
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public Integer getnContacto() {
        return nContacto;
    }
    public void setnContacto(Integer nContacto) {
        this.nContacto = nContacto;
    }
    public Integer getCedula() {
        return cedula;
    }
    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }
    public String getDireccion() {
        return direccion;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
} 


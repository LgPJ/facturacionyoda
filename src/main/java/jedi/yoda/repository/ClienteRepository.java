package jedi.yoda.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import jedi.yoda.models.ClienteModel;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteModel, Long>{
    
}
